var gulp         	= require('gulp'),
    browserSync  	= require('browser-sync').create(),
    jshint 		   	= require('gulp-jshint'),
    notify 		   	= require('gulp-notify'),
	growl 		   	= require('gulp-notify-growl'),
	jscs 		   	= require('gulp-jscs'),
    zip          	= require('gulp-zip'),
    karma        	= require('karma').server,
    concat       	= require('gulp-concat'),
    clean        	= require('gulp-clean'),
    runSequence  	= require('run-sequence'),
    es 				= require('event-stream'),
	watch 			= require('gulp-watch');

gulp.task('serve', ['browser-sync'], function() {
  // reload all Browsers
  watch(['./resources/**/*.*', '!./resources/shared/**/*'], browserSync.reload);
});

gulp.task('browser-sync', function() {
	browserSync.init({
		server: {
			baseDir: "./"
		},
		port: 8080,
		weinre: {
			port: 8081
		}
	});
});

var pkg = require('./package');
var jshintConfig = pkg.jshintConfig;

jshintConfig.lookup = false;

gulp.src('yo').pipe(jshint(jshintConfig));

//http://jshint.com/docs/options/
gulp.task('code-lint', function() {
  return gulp.src(['./resources/**/*.js', '!./resources/vendor/**/*'])
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(jshint.reporter('fail'));
});

//http://jscs.info/rules
gulp.task('code-styling', function() {
    gulp.src(['./resources/**/*.js', '!./resources/vendor/**/*'])
        .pipe(jscs())
        .pipe(jscs.reporter())
        .pipe(jscs.reporter('fail'));
});

gulp.task('unit', function (done) {
  karma.start({
    configFile: __dirname + '/tests/karma.conf.js',
    singleRun: false
  }, done);
});

gulp.task('buildpack', function() {

  gulp.task('removeTempFolder', function () {
    return gulp .src('.tmp/', {read: false})
                .pipe(clean());
  });

  gulp.task('buildZipByTempFolder', function(){
      return gulp .src(['.tmp/**'])
                  .pipe(zip('buildpack.dwp'))
                  .pipe(gulp.dest('./'));

  });

  gulp.task('disposeToupload', function(cb) {
	    return es.concat(
	        gulp.src('resources/**')
	            .pipe(gulp.dest('.tmp/resources/')),
	        gulp.src(['environment.json', 'model.jdr'])
	            .pipe(gulp.dest('.tmp/'))
	    );
  });

  runSequence('disposeToupload',
              'buildZipByTempFolder',
              'removeTempFolder');
});
