/**
 * @ngdoc function
 * @name decisyonBootstrApp
 * @description
 * This is a main bootstrap function for dcyApp
 * @param {object} window - browser window reference
 * @param {object} angular - browser window.angular reference
 */
(function decisyonBootstrApp() {
  'use strict';

  var angularApp = {};

  /**
   * Define a new angular module
   */
  angularApp.dcyApplication = angular.module('dcyApp', [
    'ngMaterial',
    'ngSanitize',
    'ngResource',
    'pascalprecht.translate',
    
    'dcyApp.constants',
    'dcyApp.providers',
    'dcyApp.services',
    'dcyApp.filters',
    'dcyApp.factories',
    'dcyApp.directives',
    'dcyApp.controllers',

    'dcyApp.ext.templates'
  ]);

  function dcyApplicationConfig(
    $controllerProvider,
    $compileProvider,
    $filterProvider,
    $provide,
    $httpProvider,
    $logProvider,
    $mdThemingProvider,
    $mdIconProvider,
    dcyConstants,
    $translateProvider,
    dcyTranslateProvider) {
    
    $httpProvider.interceptors.push('templateInterceptor');

    $logProvider.debugEnabled(window.dcyAngular.SESSION_DATA.data.applicationInfo.isInDevelopmentOrTesting);

    var registerObj = {
      controller: $controllerProvider.register,
      directive: $compileProvider.directive,
      filter: $filterProvider.register,
      provider: $provide.provider,
      factory: $provide.factory,
      service: $provide.service,
      constant: $provide.constant
    };
    
    $mdThemingProvider.definePalette('dcyDefaultGrey', $mdThemingProvider.extendPalette('grey', {
    	'50' : 'FFFFFF', // Serve a impostare lo sfondo bianco nei pannelli che altrimenti risultarebbero grigi come l'header
    }));
    
    $mdThemingProvider.definePalette('dcyToolbarGrey', $mdThemingProvider.extendPalette('grey', {
    	'A100' : 'F5F5F5',
    }));
    
    $mdThemingProvider.theme('default')
    	.primaryPalette('teal')
    	.accentPalette('blue')
    	.backgroundPalette('dcyDefaultGrey');    
    
    $mdThemingProvider.theme('toolbarTheme')
	.primaryPalette('grey', {
		'default' : '100'
	})
	.backgroundPalette('dcyToolbarGrey');
    
    $mdThemingProvider.theme('circularProgressTheme')
	.primaryPalette('grey', {
      'default': '300'
    });
    
    /* Theme used on templates catalogue */
    $mdThemingProvider.theme('blueToolbar')
	.primaryPalette('blue')
	.accentPalette('grey', {
		'default' : '100'
	});
	
    $mdIconProvider.iconSet('dcyMaterialIcons', dcyConstants.DEVEL_PATH + 'assets/images/dcy-material-icons.svg', 24);
    angularApp.dcyApplication.register = registerObj;
    angular.module('dcyApp.ext.templates').register = registerObj;
    
    /**
     * Extend ORDER-BY angular filter to manage object.
     * This routine works trasforming input object in array. 
     */
    $provide.decorator('orderByFilter', ['$delegate', function($delegate) {
        var srcFilter = $delegate;

        var extendsFilter = function(collection, expression, reverse, comparator) {
        	//If 'collection' is not an array but it is an object trasform it to Array
        	if(!angular.isArray(collection) && angular.isObject(collection)){
        		arguments[0] = _.toArray(collection);
        	}
        	
        	return srcFilter.apply(this, arguments);
        }

        return extendsFilter;
      }]);
    
    dcyTranslateProvider.translateProvider = $translateProvider;
    //$translateProvider.useLoader('dcyTranslateLoaderFactory', {});
    //$translateProvider.preferredLanguage(LANGUAGES.getLanguage());
    $translateProvider.preferredLanguage('en');
  }

  function dcyApplicationRun(dcyUtilsFactory, dcyService, dcyConstants) {
    
	  //Update the Decisyon Api Object declared into DSH_PAGE/RootPage.
	  /* *********************************************** */
	  window.DECISYON = window.DECISYON ? window.DECISYON : rootPage.DECISYON_CORE.api();
	  /* *********************************************** */

	  window.DECISYON.ng.register = {					
			  controller : function(controller_name, constructor){
				  return dcyUtilsFactory.registerComponent(controller_name, constructor, 'controller', true);
			  },
			  constant : function(constant_name, constructor){
				  return dcyUtilsFactory.registerComponent(constant_name, constructor, 'constant', true);
			  },
			  provider : function(provider_name, constructor){
				  return dcyUtilsFactory.registerComponent(provider_name, constructor, 'provider', true);
			  },
			  service : function(service_name, constructor){
				  return dcyUtilsFactory.registerComponent(service_name, constructor, 'service', true);
			  },
			  filter : function(filter_name, constructor){
				  return dcyUtilsFactory.registerComponent(filter_name, constructor, 'filter', true);
			  },
			  factory : function(factory_name, constructor){
				  return dcyUtilsFactory.registerComponent(factory_name, constructor, 'factory', true);
			  },
			  directive : function(directive_name, constructor){
				  return dcyUtilsFactory.registerComponent(directive_name, constructor, 'directive', true);
			  },
			  module : function(module_name){
				  return dcyUtilsFactory.registerComponent(module_name, null, 'module', true);
			  }
	  };

	  window.DECISYON.i18n = {
			  getTranslation 			: dcyService.getTranslation,
			  addTranslations			: dcyService.addTranslations
	  };		

	  window.DECISYON.alpha.addWatchToParam = function(window, paramName, callback){
		  try{
			  if(typeof window !== 'object' || typeof callback !== 'function' || typeof window.paramsWatched[paramName] === 'undefined'){
				  console.error('[addWatchToParam] => The parameters are not valid for attach the listener on change event.');
				  return false;
			  }
			  window.paramsWatched.dcy_watch(paramName,function(keyToChange, oldValue, newValue) {
				  callback(keyToChange, oldValue, newValue);
			  });
			  return true;
		  }
		  catch(e){
			  console.error('[addWatchToParam] => This Api is not avaible in this context.It is avaible only on mashboard.');
			  return false;
		  }
	  };

  }

  /**
   * Apply config to main module
   */
  angularApp.dcyApplication
    .config([
      '$controllerProvider',
      '$compileProvider',
      '$filterProvider',
      '$provide',
      '$httpProvider',
      '$logProvider',
      '$mdThemingProvider',
      '$mdIconProvider',
      'dcyConstants',
      '$translateProvider',
      'dcyTranslateProvider',
      dcyApplicationConfig
    ]);
  

  angularApp.dcyApplication.run(['dcyUtilsFactory', 'dcyService', 'dcyConstants', dcyApplicationRun]);

  return angularApp.dcyApplication;

}());
