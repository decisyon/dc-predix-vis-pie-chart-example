(function() {
    'use strict';

    function DCPieChartController($scope, $timeout, $q) {
        $scope.DECISYON.target.registerDataConnector(function(requestor) {
            var defered = $q.defer();

            $timeout(function() {
                defered.resolve(
                    [{
                        "x": 15,
                        "y": "IPA"
                    }, {
                        "x": 1,
                        "y": "Pils"
                    }, {
                        "x": 1,
                        "y": "Lager"
                    }, {
                        "x": 8,
                        "y": "Lambic"
                    }, {
                        "x": 12,
                        "y": "Stout"
                    }, {
                        "x": 7,
                        "y": "Pale Ale"
                    }, {
                        "x": 9,
                        "y": "Porter"
                    }, {
                        "x": 4,
                        "y": "Heffeweisse"
                    }]

                );


            }, 3000);
            return {
                data: defered.promise
            };
        });
    }

    DCPieChartController.$inject = ['$scope', '$timeout', '$q'];
    DECISYON.ng.register.controller('dcPieChartCtrl', DCPieChartController);

}());